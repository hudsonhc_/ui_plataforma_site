$(function(){


	/*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaque").owlCarousel({
		items : 1,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});

	//CARROSSEL DE MODELOS PARTICIPANTES CIMA
	$("#carouselModelsUp").owlCarousel({
		items : 4,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
            768:{
            	items:3
            },
           
            991:{
                items:3
            },
            1100:{
                items:4
            },
            1440:{
                items:4
            },
            			            
        }		    		   		    
	    
	});

	//CARROSSEL DE MODELOS PARTICIPANTES CIMA
	$("#carouselModelsDown").owlCarousel({
		items : 4,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
            768:{
            	items:3
            },
           
            991:{
                items:3
            },
            1100:{
                items:4
            },
            1440:{
                items:4
            },
            			            
        }		    		   		    
	    
	});
	//CARROSSEL DE PARCEIROS
	$("#partnersCarousel").owlCarousel({
		items : 4,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
            768:{
            	items:3
            },
           
            991:{
                items:3
            },
            1100:{
                items:4
            },
            1440:{
                items:4
            },
            			            
        }		    		   		    
	    
	});

    $(".pg-inicial section.mosaicLayers .hexagonsLists .hexagon a").click(function(e){
        event.preventDefault();
        $(".pg-modal").addClass("abrirModal");
        $("body").addClass("travarScroll");
    });

    $(".pg-modal .infoPagina span.fecharModal").click(function(e){
        $(".pg-modal").removeClass("abrirModal");
        $("body").removeClass("travarScroll");
    })

      $(".pg-inicial section.projectInfo .antiSkew .participatingModels .carouselModels .item   ").click(function(e){
        
        $(".pg-modal-modelo").addClass("abrirModal");
        $("body").addClass("travarScroll");
    });

    $(".pg-modal-modelo .infoPagina span.fecharModal").click(function(e){
        window.location="file:///C:/wamp/www/ui/ui_plataforma_site/index.html";
        $("body").removeClass("travarScroll");
    })



	// $(document).ready(function () {
    //        $("html").niceScroll();
    //    });
	
    // $(".linkLoja").mouseover(function(){
    //     $(this).next().fadeIn();
    // });

    // $(".linkLoja").mouseout(function(){
    //     $(this).next().fadeOut();
    // });
});